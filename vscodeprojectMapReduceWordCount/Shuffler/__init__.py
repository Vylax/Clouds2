# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging

def main(pairList: list[tuple[str,int]]) -> list[tuple[str, list[int]]]:
    inputToReduce=[]
    while(len(pairList)>0):
        tail = pairList.pop()
        foundkey=False
        for i in range(len(inputToReduce)):
            if(tail[0] == inputToReduce[i][0]):
                inputToReduce[i][1].append(tail[1])
                foundkey = True
                break
        if(not foundkey):
            inputToReduce.append((tail[0],[tail[1]]))
    return inputToReduce
