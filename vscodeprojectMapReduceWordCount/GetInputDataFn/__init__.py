# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import os, uuid
from azure.identity import DefaultAzureCredential
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient

def main(name: str) -> list[tuple[int, str]]:

    output:list[tuple[int, str]] = []
    files: list[str] = []
    try:
        connect_str = "DefaultEndpointsProtocol=https;AccountName=cloudsassignemnt2mr;AccountKey=dG4O+YEYOOyxZPymlTYEi3fWdtALb/r3cbF0ScOdvACPRXO95EwLKodzCeHbluxfOAsVpeMXx1tI+AStg6hx5w==;EndpointSuffix=core.windows.net"
        container_name = name
        
        blob_service_client = BlobServiceClient.from_connection_string(connect_str)
        container_client = blob_service_client.get_container_client(container_name)

        blob_list = container_client.list_blobs()
        
        for blob in blob_list:
            blob_client = container_client.get_blob_client(blob.name)
            files.append(blob_client.download_blob().readall().decode('ASCII'))  #read blob content as a string and store it in files

    except Exception as ex:
        print('Exception:')
        print(ex)
        return output

    lineIndex=0
    for file in files:
        lines=file.split("\r\n")
        for line in lines:
            output.append((lineIndex, line))
            lineIndex+=1
    return output
