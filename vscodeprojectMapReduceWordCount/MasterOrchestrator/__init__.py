# This function is not intended to be invoked directly. Instead it will be
# triggered by an HTTP starter function.
# Before running this sample, please:
# - create a Durable activity function (default name is "Hello")
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import json

import azure.functions as func
import azure.durable_functions as df


def orchestrator_function(context: df.DurableOrchestrationContext):

    lines: list = yield context.call_activity("GetInputDataFn", "mrfiles")

    if not lines:
        raise Exception("Lines required as input")

    mapperTasks = []
    for line in lines:
        mapperTasks.append(context.call_activity("Mapper", line))
    mapperOutputs = yield context.task_all(mapperTasks) #(MAP) runs mapper tasks in parallel
    mapperOutputs = [num for sublist in mapperOutputs for num in sublist] #Aggregate over all the mappers output

    inputToReduce = yield context.call_activity("Shuffler", mapperOutputs) #(SHUFFLE)
    
    reducerTasks = []
    for pair in inputToReduce:
        reducerTasks.append(context.call_activity("Reducer", pair))
    reducedOutput = yield context.task_all(reducerTasks) #(REDUCE) runs reducer tasks in parallel
    
    return reducedOutput

main = df.Orchestrator.create(orchestrator_function)