import numpy as np
from flask import Flask

app = Flask(__name__)

#Evaluates the numerical integration of function f on the interval [lower,upper] by using N subintervals
def numericalIntegration(N, lower, upper, f):
    step = (upper-lower)/N
    res = 0
    for i in range(N):
        xi = lower + step*(i+0.5)
        res += f(xi)*step
    return res

@app.route("/integral/<lower>/<upper>")
def main(lower, upper):
    res = "<p>"
    for p in range(7):
        res += f'N={10**p}, I={numericalIntegration(10**p, float(lower), float(upper), np.sin)}<br>'#note: we don't specify variables types in the route method so that we don't have to setup overloads (which would add 3 more methods and routes) because python is so permisive that it will try to parse anything into a float if we ask it to do so
    return res + "</p>"