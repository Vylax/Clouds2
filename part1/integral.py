import math

#Evaluates the numerical integration of function f on the interval [lower,upper] by using N subintervals
def numericalIntegration(N, lower, upper, f):
    step = (upper-lower)/N
    res = 0
    for i in range(N):
        xi = lower + step*(i+0.5)
        res += f(xi)*step
    return res

def main(lower, upper):
    for p in range(7):
        print(f'N={10**p}, I={numericalIntegration(10**p, lower, upper, math.sin)}')

main(0, 3.14159)