import logging

import azure.functions as func

import math

def numericalIntegration(N, lower, upper, f):
    step = (upper-lower)/N
    res = 0
    for i in range(N):
        xi = lower + step*(i+0.5)
        res += f(xi)*step
    return res

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    bounds = req.params.get('bounds')
    if not bounds:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            bounds = req_body.get('bounds')

    if bounds:
        temp = bounds.split('/')
        res = ""
        for p in range(7):
            res += f"N={10**p}, I={numericalIntegration(10**p, float(temp[0]), float(temp[1]), math.sin)}\n"
        return func.HttpResponse(res)
    else:
        return func.HttpResponse(
             "This HTTP triggered function executed successfully. Pass bounds (lower/upper) in the query string or in the request body for a personalized response.",
             status_code=200
        )
